; FixCygwinConnectorKeys.ahk
;
; This script is a workaround to fix a bug regarding the capture of certain keys under ConEmu/Cygwin.
; @see https://github.com/Maximus5/cygwin-connector/issues/3
;
; This is a list of Readline movement commands:
; @see http://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Readline-Movement-Commands
;
; This is a list of the key codes used by AutoHotkey:
; @see https://www.autohotkey.com/docs/KeyList.htm


; General configuration.
#SingleInstance force
Menu, Tray, Icon, FixCygwinConnectorKeys.ico


; Arrow up key sends Ctrl + p in ANSI.
Up::
SendAnsiCodeToConsole("^p", "{Up}")
return


; Arrow down key sends Ctrl + n in ANSI.
Down::
SendAnsiCodeToConsole("^n", "{Down}")
return


; Arrow left key sends Ctrl + b in ANSI.
Left::
SendAnsiCodeToConsole("^b", "{Left}")
return


; Arrow right key sends Ctrl + f in ANSI.
Right::
SendAnsiCodeToConsole("^f", "{Right}")
return


; Home key sends Ctrl + a in ANSI.
Home::
SendAnsiCodeToConsole("^a", "{Home}")
return


; End key sends Ctrl + e in ANSI.
End::
SendAnsiCodeToConsole("^e", "{End}")
return


; AltGr + @ sends the Unicode character 0040 in ANSI.
; @see https://www.fileformat.info/info/unicode/char/0040/index.htm
<^>!@::
SendAnsiCodeToConsole("{U+0040}", "@")
return


; Sends an ANSI code to the ConEmu console.
;
; @param ansiCode ANSI code to send to the console if it is active.
; @param normalCode Normal key code to send if the console it is not active.
SendAnsiCodeToConsole(ansiCode, normalCode) {
    if (WinExist("ahk_class VirtualConsoleClass")) {
        if (WinActive("ahk_class VirtualConsoleClass")) {
            Send %ansiCode%
        } else {
            Send %normalCode%
        }
    } else {
        Send %normalCode%
    }
}